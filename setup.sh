#!/usr/bin/env bash
/root/setup_tls.sh;

if [ -n "$POSTFIX_RELAY_HOST" ]; then
    echo "relayhost = ${POSTFIX_RELAY_HOST}" >> /etc/postfix/main.cf
fi

if [ -n "$POSTFIX_REQUIRE_TLS" ]; then
    echo "smtpd_tls_security_level = encrypt" >> /etc/postfix/main.cf
else
    echo "smtpd_tls_security_level = may" >> /etc/postfix/main.cf
fi

if [ -n "$POSTFIX_OPENDKIM_SRV"]; then
echo "
milter_default_action = accept
milter_protocol = 2
smtpd_milters = ${POSTFIX_OPENDKIM_SRV}
non_smtpd_milters = ${POSTFIX_OPENDKIM_SRV}
" >> /etc/postfix/main.cf
fi

/usr/sbin/rsyslogd;
/usr/sbin/postfix start 2>&1;

#we go too fast and mail.log doesn't exist yet
sleep 1;
tail -F /var/log/maillog;