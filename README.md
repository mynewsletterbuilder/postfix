# postfix

This is a TLS enabled postfix server.

## config

use the following env variable to configure:

`POSTFIX_RELAY_HOST` - address of a relay host, if required.
`POSTFIX_REQUIRE_TLS` - set to true to force TLS from clients

## postfix:test

The test tag contains a dummy postfix servers.  It discards any mail sent to it.

If you want to check the received message you can send to `save@[hostname]` and it will save messages to `/var/log/mail.output`

If you want the message to bounce send it to `bounce@[hostname]` and it will 550 bounce.