#!/usr/bin/env bash

if [ -z "$POSTFIX_HOSTNAME" ]; then
    $POSTFIX_HOSTNAME=$(hostname)
fi

cd /etc/ssl/private;
# genrsa -des3 -out localhost.key 2048;
# openssl req -new -key localhost.key -out localhost.csr;
# openssl x509 -req -days 365 -in localhost.csr -signkey localhost.key -out localhost.crt;

echo "setting up ssl with hostname: ${POSTFIX_HOSTNAME}";
openssl req \
    -new \
    -newkey rsa:4096 \
    -days 365 \
    -nodes \
    -x509 \
    -subj "/C=US/ST=NC/L=Asheville/O=massmail/CN=${POSTFIX_HOSTNAME}" \
    -keyout localhost.key \
    -out localhost.cert;

cat localhost.key localhost.cert > localhost.pem;

echo ${POSTFIX_HOSTNAME} > /etc/mailname;
echo "myhostname = ${POSTFIX_HOSTNAME}" >> /etc/postfix/main.cf;
echo "set postfix mailname to: $(cat /etc/mailname)";